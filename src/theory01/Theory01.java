package theory01;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
/*
page 31 slide CH3
dfa for L(((aa*)*b)*)
 */
public class Theory01 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../style/sample.fxml"));
        primaryStage.setTitle("Theory 01");
        primaryStage.setScene(new Scene(root, 550, 365));
        primaryStage.getScene().getStylesheets().add(Theory01.class.getResource("../style/style.css").toExternalForm());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
